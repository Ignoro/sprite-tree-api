package spriteTree;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

class JsonMacro {
	static var pos: Position;
	static var inClass: ClassType;

	macro public static function buildTypes(path: String) : Array<Field> {
		pos = Context.currentPos();
		inClass = Context.getLocalClass().get();

		var json = load(path);
		var fields = Context.getBuildFields();

		var def = macro class Tmp {};
		def.name = "SpriteData";
		def.kind = TDStructure;
		def.fields = new Array();
		def.pack = inClass.pack.copy();

		for (f in Reflect.fields(json)) {
			var sprite = Reflect.getProperty(json, f);
			var anchors = Reflect.getProperty(sprite, "anchors");
			var typeName = "T" + f;

			makeSpriteAnchorType(typeName, anchors);

			var param = TPType (TPath ({ name : typeName, pack : new Array() }));

			var field : Field = {
				name: f,
				access: [],
				kind: FVar(TPath ({ name : "SpriteData", sub : "SaveData", pack : [ "spriteTree" ], params : [ param ] }), null),
				pos: pos
			}

			def.fields.push(field);
		}

		haxe.macro.Context.defineType(def);

		var field : Field = {
			name: "spriteData",
			access: [APublic, AStatic],
			kind: FVar(TPath ({ name : def.name, pack : new Array() }), macro $v{json}),
			pos: pos
		}

		fields.push(field);

		return fields;
	}

	static function makeSpriteAnchorType(name: String, v : Dynamic) {
		var def = macro class Tmp {};
		def.name = name;
		def.kind = TDStructure;
		def.fields = new Array();
		def.pack = inClass.pack.copy();

		for (f in Reflect.fields(v)) {
			var field : Field = {
				name: f,
				access: [],
				kind: FVar(TPath ({ name : "SpriteData", sub : "Anchor", pack : [ "spriteTree" ] }), null),
				pos: pos
			}

			def.fields.push(field);
		}

		haxe.macro.Context.defineType(def);
	}


	static function load(path : String) {
		return try {
			var json = haxe.Json.parse(sys.io.File.getContent(path));
			return json;
		} catch (e) {
			haxe.macro.Context.error('Failed to load json: $e', haxe.macro.Context.currentPos());
		}
	}
}
#end
