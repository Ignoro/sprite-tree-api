package spriteTree;


typedef Point = {
	var x : Float;
	var y : Float;
}

typedef Anchor = {
	var name : String;
	var point : Point;
	var angle : Float;
}

typedef SpriteData = {
	var name : String;
	var position : Point;
	var scale : Float;
	var anchors : Array<Anchor>;
}

typedef SaveData<T> = {
	var name : String;
	var position : Point;
	var scale : Float;
	var anchors : T;
}

class Utils {
#if (!js)
	static public function save(sprites : Map<String, SpriteData>, fileName) : Void {
		var data = dataToJson(sprites);
		sys.io.File.saveContent(fileName+".json", data);
	}

	static public function load(sprites : Map<String, SpriteData>, fileName) {
		var data = try { sys.io.File.getContent(fileName+".json"); } catch (_) null;

		if (null == data)
			return sprites;

		return jsonToData(data);
	}
#end


	static function dataToJson(value : Map<String, SpriteData>) : String {
		var sprites = new Array();
		for (i in value.keyValueIterator()) {
			i.value.anchors.sort((e1, e2) -> {
				var a1 = Math.atan2(e1.point.y, e1.point.x);
				var a2 = Math.atan2(e2.point.y, e2.point.x);
				var d = a1 - a2;
				return d > 0 ? 1 : d < 0 ? -1 : 0;
			});
			var anchors = {};
			for (f in i.value.anchors) {
				Reflect.setField(anchors, f.name, f);
			}
			var value : SaveData<Dynamic> = { name : i.value.name, position : i.value.position, scale : i.value.scale, anchors : anchors };
			sprites.push(value);
		}
		sprites.sort((e1, e2) -> e1.name < e2.name ? -1 : 1);
		var data = {};
		for (f in sprites) {
			var r = ~/\./;
			var k = r.replace(f.name, "_");
			Reflect.setField(data, k, f);
		}
		var data = haxe.Json.stringify(data, "\t");
		return data;
	}

	static function jsonToData(data : String) : Map<String, SpriteData> {
		var data = haxe.Json.parse(data);

		var value = new Map();
		for (f in Reflect.fields(data)) {
			var sd = Reflect.getProperty(data, f);
			var anchors = new Array();
			for (af in Reflect.fields(sd.anchors)) {
				var a : Anchor = Reflect.getProperty(sd.anchors, af);
				anchors.push(a);
			}
			var data = { name : sd.name, position : sd.position, scale : sd.scale, anchors : anchors }
			value.set(sd.name, data);
		}

		return value;
	}
}
